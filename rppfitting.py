import numpy as np
import numba
import numba.experimental
import math
from fxpmath import Fxp
from typing import List
from numba.typed import Dict
from numba.core import types

@numba.jit(nopython=True)
def expFit(x, amp, decay1, offset):
	"""
	This function defines a simple exponentially decaying function that is used for fitting the decay rate of waveforms
	
	Parameters
	----------
	x: np.ndarray
		the x coordinates of the dataset
	amp: int, float
		the amplitude of the function
	decay1: int, float
		the main decay constant of the waveform
	offset: int, float
		the baseline offset of the waveform
	
	Returns
	-------
	waveform: np.ndarray
		The waveform with those parameters
	"""
	out = amp * np.exp(-x/decay1) + offset
	return out

'''
Define all of the functions relevant to the trapezoidal filter
'''
def defineSingleTrap(rise, top, tau, length=None):
	"""
	This function defines the standard trapezoidal filter as discussed in the Jordanov paper in such a way as to be used in a convolution based filtering approach. That paper defines it for recursive implementations, this is a convolution based implementation more suited for Python and use with convolution functions such as numpy's np.convolve or np.fftconvlve.

	Nuclear Instruments and Methods in Physics Research A353 (1994) 261-264
	https://deepblue.lib.umich.edu/bitstream/handle/2027.42/31113/0000009.pdf?sequence=1

	Parameters
	----------
	rise: int
		this parameter controls the length of the rising edge of the trapezoid. This should be tuned and optimized based on the noise in the system. A good initial guess is the same value as tau.
	top: int
		the length of the flat top of the trapezoid. This should be longer than the expected rising edge of the pulse shape to properly integrate accumulated charge.
	tau: int, float
		the decay constant of the electronics. This should be a fixed value determined by the electronics chain. This can be determined by fitting the decaying region of multiple waveforms to an exponentially decaying function and extracting the decay rate
	length: int (defaults to None)
		the desired length of the filter. This needs to be at least 2 * rise + top and can be longer if padding is desired for the filtering process (some convolution based filtering methods need the filter and waveform to be the same size)
		If no input is provided, this defaults to None and the filter is not padded
	Returns
	-------
	filter: np.ndarray
		This returns the filter defined in a numpy array ready to be convolved with the waveform. It is pre-scaled such that the output from using this filter with a normalized convolution methodology will not artificially scale the waveform amplitude. With some FFT-based convolution techniques, additional scaling may be necessary if the method isn't already normalized.
	"""
	filt = None
	if length is None:
		filt = np.zeros(rise * 2 + top)
	else:
		if length < rise * 2 + top:
			print('invalid length parameter')
			print('should be longer than rise * 2 + top')
			return None
		else:
			filt = np.zeros(length)
	for i in range(rise):
		filt[i]=i+tau
		filt[i+rise+top]=rise-tau-i
	for i in range(rise,rise+top):
		filt[i]=rise
	for i in range(rise+rise+top,len(filt)):
		filt[i]=0
	scale=1.0/(rise*tau)
	filt*=scale
	return filt

spec = [
	('data', numba.float64[:]),
	('index', numba.int64),
	('delay', numba.float64)
]
@numba.experimental.jitclass(spec)
class VarDelay:
	def __init__(self,delay):
		self.data = np.zeros(delay, dtype=numba.float64)
		self.index = 0
		self.delay = delay
	def Val(self,value):
		if self.delay == 0:
			return value
		else:
			out = self.data[self.index]
			self.data[self.index] = value
			self.index+=1
			if self.index >= self.delay:
				self.index=0
			return out

spec = [('val', numba.float64)]
@numba.experimental.jitclass(spec)
class Acc:
	def __init__(self):
		self.val = 0
	def add(self, val):
		self.val += val
		return self.val

@numba.jit(nopython=True)
def stirling(n,k):
    n1=n
    k1=k
    if n == 0 and k == 0:
        return 1
    if n > 0 and k == 0:
        return 0
    if k > n:
        return 0
    else:
        return stirling(n-1, k-1) + (n - 1) * stirling(n - 1, k)

@numba.jit(nopython=True)
def factorial(n):
    if n <= 1:
        return 1
    else:
        return n * factorial(n-1)

@numba.jit(nopython=True)
def calculateCPrimes(Cs):
    numCs = len(Cs)
    matrix = np.identity(numCs)
    for n in range(numCs):
        for k in range(n+1):
            matrix[n, k] = stirling(n,k)/factorial(n)
    matrix = matrix.T
    inverse = np.linalg.inv(matrix)
    out = np.dot(inverse, Cs)
    return out

@numba.jit(nopython=True)
def calculateLambdas(order, length):
    if order == 0:
        return np.array([1.0])
    else:
        out = []
        for i in range(order+1):
            if i == 0:
                out.append(1.0)
            else:
                t = 0
                for j in range(0, length+1):
                    t += stirling(i, j)*length**j
                out.append(t/factorial(i))
        return np.array(out)

@numba.jit(nopython=True)
def calcFitParams(region, order):
    matrix = np.zeros((len(region), order))
    x = np.arange(len(region))+1
    for i in range(order):
        matrix[:,i] = np.power(x, i)
    inverse = np.linalg.pinv(matrix)
    params = np.dot(inverse, region)
    return params


spec = [
    ('order', numba.int64),
    ('cPrimes', numba.float64[:]),
    ('delay', numba.int64),
    ('lambdas', numba.float64[:])
]
class Filter:
    def __init__(self, region, order):
        self.order = order
        x = np.arange(len(region))+1
        params = calcFitParams(region, order)
        self.cPrimes = calculateCPrimes(params)[:]
        self.delay = len(x)
        self.lambdas = calculateLambdas(self.order, self.delay)

@numba.jit(nopython=True)
def arbOrder(inp, delay, lambdas, cPrimes):
	if len(lambdas) != len(cPrimes):
		return None
	else:
		order = len(cPrimes)
		out = []
		delayL = VarDelay(delay)
		accs = []
		for i in range(order):
			accs.append(Acc())
		for val in inp:
			delL = delayL.Val(val)
			t = []
			for i in range(order):
				if i == 0:
					t.append(accs[0].add(val-lambdas[0]*delL))
				else:
					t.append(accs[i].add(t[i-1]-lambdas[i]*delL))
			tout = 0
			for i in range(order):
				tout += t[i]*cPrimes[i]
			out.append(tout)
		return out

@numba.jit(nopython=True)
def arbOrderArbNumber(inp, filters):
	numFilts = len(filters)
	orders = []
	delays = []
	filtAccs = []
	cPrimes = []
	lambdas = []
	for filt in filters:
		accs = []
		order = int(filt['order'][0])
		orders.append(order)
		for i in range(order+1):
			accs.append(Acc())
		filtAccs.append(accs)
		delays.append(VarDelay(int(filt['delay'][0])))
		cPrimes.append(filt['cPrimes'])
		lambdas.append(filt['lambdas'])
	out = []
	for val in inp:
		delLs = [delays[0].Val(val)]
		for i in range(1, numFilts):
			delLs.append(delays[i].Val(delLs[-1]))
		outs = 0
		for f in range(numFilts):
			if f == 0:
				val = val
			else:
				val = delLs[f-1]
			t = []
			for i in range(orders[f]):
				if i == 0:
					t.append(filtAccs[f][0].add(val-lambdas[f][0]*delLs[f]))
				else:
					t.append(filtAccs[f][i].add(t[i-1]-lambdas[f][i]*delLs[f]))
			tout = 0
			for i in range(orders[f]):
				tout += t[i]*cPrimes[f][i]
			outs += tout
		out.append(outs)
	return out

@numba.jit(nopython=True)
def Filter(region, order):
    x = np.arange(len(region))+1
    params = calcFitParams(region, order)
    delay = len(x)
    d = Dict.empty(
        key_type = types.unicode_type,
        value_type = types.float64[:],
    )
    d['order'] = np.asarray([order], dtype=np.float64)
    d['cPrimes'] = np.asarray(calculateCPrimes(params), dtype=np.float64)
    d['delay'] = np.asarray([len(x)], dtype=np.float64)
    d['lambdas'] = np.asarray(calculateLambdas(order, delay))
    return d

def defineFilters(inpFilter, segments, orders):
    out = []
    for seg, order in zip(segments, orders):
        region = inpFilter[seg[0]:seg[1]]
        x = np.arange(len(region))+1
        out.append(Filter(region, order))
    return out


def arbOrderArbNumberFixed(inp, filters, n_word = 32, n_frac = 16):
    numFilts = len(filters)
    orders = []
    delays = []
    filtAccs = []
    cPrimes = []
    lambdas = []
    for filt in filters:
        accs = []
        order = int(filt['order'][0])
        orders.append(order)
        for i in range(order+1):
            accs.append(Acc())
        filtAccs.append(accs)
        delays.append(VarDelay(int(filt['delay'][0])))
        cPrimes.append(filt['cPrimes'])
        lambdas.append(filt['lambdas'])
    out = []
    for val in inp:
        delLs = [delays[0].Val(val)]
        for i in range(1, numFilts):
            delLs.append(delays[i].Val(delLs[-1]))
        outs = 0
        for f in range(numFilts):
            if f == 0:
                val = val
            else:
                val = delLs[f-1]
            t = []
            for i in range(orders[f]):
                if i == 0:
                    blah = Fxp(float(lambdas[f][0]), signed=True, n_word=n_word, n_frac = n_frac)*Fxp(float(delLs[f]), signed=True, n_word=n_word, n_frac=n_frac)
                    t.append(filtAccs[f][0].add(val-blah()))
                else:
                    blah = Fxp(float(lambdas[f][i]), signed=True, n_word=n_word, n_frac=n_frac)*Fxp(float(delLs[f]), signed=True, n_word=n_word, n_frac=n_frac)
                    t.append(filtAccs[f][i].add(t[i-1]-blah()))
            tout = 0
            for i in range(orders[f]):
                blah = Fxp(float(t[i]), signed=True, n_word=n_word, n_frac=n_frac)*Fxp(float(cPrimes[f][i]), signed=True, n_word=n_word, n_frac=n_frac)
                tout += blah()
            outs += tout
        out.append(outs)
    return out